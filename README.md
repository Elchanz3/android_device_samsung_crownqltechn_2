# Device Tree for Samsung Galaxy Note 9 (crownqltechn)

The Samsung Galaxy Note 9 (codenamed "crownqltechn") is a top of the line device from Samsung. It was announced in August 2018 and was released in August 2018.


## Device specifications

| Feature                 | Specification                                                   |
| :---------------------- | :---------------------------------------------------------------|
| Chipset                 | Qualcomm SDM845 Snapdragon 845                                  |
| CPU                     | Octa-core (4x2.8 GHz Kryo 385 Gold & 4x1.7 GHz Kryo 385 Silver) |
| GPU                     | Adreno 630                                                      |
| Memory                  | 6/8 GB                                                            |
| Shipped Android Version | 8.0 (OneUI 2.1)                                                 |
| Storage                 | 128/512 GB UFS 2.1                                               |
| SIM                     | Dual SIM (Nano-SIM, dual stand-by)                              |
| MicroSD                 | Up to 400 GB                                                    |
| Battery                 | 4000 mAh Li-Ion (non-removable)                                 |
| Dimensions              | 161.9 x 76.4 x 8.8 mm                                           |
| Display                 | 6.4 inch, 1440 x 2960 (18.5:9 ratio)                            |
| Dual Main Camera        | 12 MP, f/1.5-2.4, 26mm (wide), 1/2.55", 1.4µm, PDAF, OIS        |
|                         | 12 MP, f/2.4, 52mm (telephoto), 1/3.6", 1.0µm, AF, OIS          |
| Dual Front Camera       | 8 MP, f/1.7, (wide), 1/3.6", 1.22µm, AF                         |
|                         | 2 MP (dedicated iris scanner camera)                            |
| Fingerprint             | Rear-mounted                                                    |
| Sensors                 | Iris scanner, Accelerometer, Gyro, Proximity, Compass...        |

## Device picture

![Samsung Galaxy Note 9](https://fdn2.gsmarena.com/vv/pics/samsung/samsung-galaxy-note9-r2.jpg)

Copyright (C) 2023 lineageK.
